package vn.com.nat.track_location_android.ui.component.map;


import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.BaseActivity;
import vn.com.nat.track_location_android.ui.base.listeners.LocationMonitoringService;
import vn.com.nat.track_location_android.ui.component.home.HomeActivity;
import vn.com.nat.track_location_android.utils.ObjectUtil;

/**
 * Created by NAT on 16/06/2019
 */

public class MapActivity extends BaseActivity implements MapContract.View, OnMapReadyCallback {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    @Inject
    MapPresenter presenter;

    @Nullable
    @BindView(R.id.imgBack)
    ImageView imgBack;

    @Nullable
    @BindView(R.id.tvAddress)
    TextView tvAddress;

    @Nullable
    @BindView(R.id.rlComplete)
    RelativeLayout rlComplete;

    boolean isNewOrder = false;
    Order order;

    private View mapView;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private Location location;
    private LatLng myLatLong;
    private List<Polyline> polylinePaths = new ArrayList<>();
    private List<LatLng> latLngList = new ArrayList<>();
    private BroadcastReceiver broadcastReceiver;
    private AlertDialog alertGPS;
    private boolean isOpenSetting = false;


    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(MapActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        presenter.getExtra(this);
        presenter.setActivity(this);
        setupMapFragment();
        checkGPS();//check status of GPS
        if (isNewOrder) {
            initLocalBroadcastManager();//broadcast listener change location
            startService();
        } else {
            rlComplete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_map;
    }

    @OnClick({R.id.imgBack, R.id.rlComplete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack: {
                onBackPressed();
                break;
            }
            case R.id.rlComplete: {
                handleCompleteOrder();
                break;
            }
        }
    }

    @Override
    public void setupMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void enableMyLocationIfPermitted() {
        isOpenSetting = false;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (ObjectUtil.isNull(location)) {
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (location != null) {
                    myLatLong = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLatLong, 17);
                    mMap.animateCamera(cameraUpdate);
                    CameraPosition myPosition = new CameraPosition.Builder()
                            .tilt(60)
                            .target(myLatLong).zoom(17).bearing(90).build();
                    mMap.animateCamera(
                            CameraUpdateFactory.newCameraPosition(myPosition));

                }
            }
        }
    }

    @Override
    public void initLocalBroadcastManager() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);
                if (!ObjectUtil.isEmptyStr(latitude) && !ObjectUtil.isEmptyStr(longitude)) {
                    Location locationCurrent = new Location("");
                    locationCurrent.setLatitude(Double.parseDouble(latitude));
                    locationCurrent.setLongitude(Double.parseDouble(longitude));
                    LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                    latLngList.add(latLng);
//                    drawPolylinePath(latLngList);//draw polyline when change location
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );
    }

    @Override
    public void drawPolylinePath(List<LatLng> latLngList) {
        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.CYAN).
                width(5);

        for (int i = 0; i < latLngList.size(); i++) {
            polylineOptions.add(latLngList.get(i));
            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableMyLocationIfPermitted();
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        //set position locationButton
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 100);
        //draw polyline if not is a new order(old order)
        if (!isNewOrder) {
            if (!ObjectUtil.isNull(this.order) && !ObjectUtil.isEmptyList(this.order.getLocate_list())) {
                drawPolylinePath(this.order.getLocate_list());
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                }
            }

        }
    }

    @Override
    public void startService() {
        Intent intent = new Intent(getApplicationContext(), LocationMonitoringService.class);
        startService(intent);
    }

    @Override
    public void stopService() {
        if (!ObjectUtil.isNull(broadcastReceiver)) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
        Intent intent = new Intent(getApplicationContext(), LocationMonitoringService.class);
        stopService(intent);
    }

    @Override
    public void handleCompleteOrder() {
        if (ObjectUtil.isNull(this.order)) {
            return;
        }
        this.order.setLocate_list(latLngList);
        this.order.setComplete_time(System.currentTimeMillis());
        App.getOrderList().add(order);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void checkGPS() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled) {
            buildAlertMessageNoGps();
        }
    }

    @Override
    public void buildAlertMessageNoGps() {
        if (alertGPS != null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_dialog_permission));
        builder.setMessage(getString(R.string.message_dialog_permission));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.button_dialog_permission), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
                isOpenSetting = true;
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
            }
        });
        alertGPS = builder.create();
        alertGPS.show();
    }

    @Override
    public void getIntentOrder(Order order, boolean isNewOrder) {
        this.order = order;
        this.isNewOrder = isNewOrder;
        if (!ObjectUtil.isNull(order)) {
            tvAddress.setText(order.getAddress());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isOpenSetting) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_PERMISSION_REQUEST_CODE);
                return;
            } else {
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableMyLocationIfPermitted();
                    }
                }, 1500);
            }

        }
    }

    @Override
    protected void onDestroy() {
        stopService();
        super.onDestroy();
    }
}
