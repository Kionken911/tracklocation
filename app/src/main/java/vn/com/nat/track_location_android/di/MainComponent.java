package vn.com.nat.track_location_android.di;


import javax.inject.Singleton;

import dagger.Component;
import vn.com.nat.track_location_android.ui.component.home.HomeActivity;
import vn.com.nat.track_location_android.ui.component.map.MapActivity;
import vn.com.nat.track_location_android.ui.component.order.OrderActivity;
import vn.com.nat.track_location_android.ui.component.splash.SplashActivity;

/**
 * Created by NAT on 5/12/2017
 */
@Singleton
@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(HomeActivity homeActivity);

    void inject(SplashActivity splashActivity);

    void inject(OrderActivity orderActivity);

    void inject(MapActivity mapActivity);
}
