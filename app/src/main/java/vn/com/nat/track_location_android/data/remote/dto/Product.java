package vn.com.nat.track_location_android.data.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import vn.com.nat.track_location_android.data.remote.dto.base.EObject;
import vn.com.nat.track_location_android.data.remote.dto.base.ERespond;
import vn.com.nat.track_location_android.utils.CommonUtils;


/**
 * Created by NAT on 16/06/2019
 */

public class Product extends EObject {

    public static class ProductRespond extends ERespond {
        Product data;

        public Product getData() {
            return data;
        }

        public void setData(Product data) {
            this.data = data;
        }
    }

    @Expose
    @SerializedName("product_id")
    private String product_id;
    @Expose
    @SerializedName("product_name")
    private String product_name;
    @Expose
    @SerializedName("price")
    private long price;

    private boolean isChoosed = false;

    public Product() {
    }

    public Product(String product_id, String product_name, long price) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.price = price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return CommonUtils.getFormatNumber((int) price);
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }
}
