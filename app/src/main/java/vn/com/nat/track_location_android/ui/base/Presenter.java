package vn.com.nat.track_location_android.ui.base;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;
import vn.com.nat.track_location_android.use_case.UserUseCase;


/**
 * Created by tuannguyen on 13/12/17.
 */


public abstract class Presenter<T extends BaseView> {

    private UserUseCase useCase;
    private WeakReference<T> view;
    protected BaseActivity activity;
    protected Intent intent;
    private SharedPreferences sharedPreferences;
    private BaseFragment fragment;

    public Presenter(UserUseCase userUseCase) {
        this.useCase = userUseCase;

    }

    protected AtomicBoolean isViewAlive = new AtomicBoolean();

    public T getView() {
        return view.get();
    }

    public void setView(T view) {
        this.view = new WeakReference<>(view);
    }

    public void setActivity(BaseActivity activity) {
        this.activity = activity;
        sharedPreferences = App.getInstance().getSettingPreference();
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
        sharedPreferences = App.getInstance().getSettingPreference();
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }


    public void initialize(Bundle extras) {
    }

    public abstract void getExtra(Activity activity);

    public void start() {
        isViewAlive.set(true);
    }

    public void finalizeView() {
        isViewAlive.set(false);
    }

}
