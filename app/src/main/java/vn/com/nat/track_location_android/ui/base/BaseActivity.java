package vn.com.nat.track_location_android.ui.base;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.AsyncResponse;
import vn.com.nat.track_location_android.ui.base.listeners.ActionBarView;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;
import vn.com.nat.track_location_android.utils.CommonUtils;
import vn.com.nat.track_location_android.utils.NetworkUtils;

public abstract class BaseActivity extends AppCompatActivity implements BaseView,
        ActionBarView, AsyncResponse {

    protected Presenter presenter;
    private Unbinder unbinder;
    private SharedPreferences sharedPreferences;
    private ProgressDialog progressDialog;


    protected abstract void initializeDagger();

    protected abstract void initializePresenter();

    public abstract int getLayoutId();

    protected void initializeFinal() {

    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        initializeDagger();
        initializePresenter();
        if (presenter != null) {
            presenter.setActivity(this);
            presenter.initialize(getIntent().getExtras());
        }
        App app = (App) getApplicationContext();
        sharedPreferences = app.getSettingPreference();
        initializeFinal();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showToast(int errMessage) {
        CommonUtils.showAppToast(this, errMessage);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.finalizeView();
        }
    }

    @Override
    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void killApplication() {
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean isInternetConnected() {
        boolean isConnected = true;
        if (!NetworkUtils.isConnected(this)) {
            isConnected = false;
            showToast(R.string.err_no_connected);
        }
        return isConnected;
    }
}

