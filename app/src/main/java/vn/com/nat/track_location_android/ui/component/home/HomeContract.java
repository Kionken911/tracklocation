package vn.com.nat.track_location_android.ui.component.home;

import java.util.List;

import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;

/**
 * Created by NAT on 16/06/2019
 */

public interface HomeContract {
    interface View extends BaseView {
        void navigateToOrderActivity();

        void navigateToMapActivity(Order order);

        void initOrderList();

        void displayOrderList(List<Order> orderList);
    }

    interface Presenter {
        void getOrderListFake();
    }
}
