package vn.com.nat.track_location_android.ui.component.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Product;

public class ProductAdapter extends ArrayAdapter<Product> {

    private Context context;
    private int resource;
    private List<Product> productList;
    private ProductItemListener listener;

    public ProductAdapter(Context context, int resource, List<Product> productList, ProductItemListener listener) {
        super(context, resource, productList);
        this.context = context;
        this.resource = resource;
        this.productList = productList;
        this.listener = listener;
    }

    public interface ProductItemListener {
        void onItemSelected(int position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = convertView.findViewById(R.id.tvName);
            viewHolder.tvPrice = convertView.findViewById(R.id.tvPrice);
            viewHolder.cbChecked = convertView.findViewById(R.id.cbChecked);
            viewHolder.rlItem = convertView.findViewById(R.id.rlItem);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Product product = productList.get(position);
        viewHolder.tvPrice.setText(product.getPrice() + context.getString(R.string.currency));
        viewHolder.tvName.setText(product.getProduct_name());
        if (product.isChoosed()) {
            viewHolder.cbChecked.setChecked(true);
        } else {
            viewHolder.cbChecked.setChecked(false);
        }
        viewHolder.rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemSelected(position);
            }
        });
        return convertView;
    }

    public class ViewHolder {
        TextView tvName, tvPrice;
        CheckBox cbChecked;
        RelativeLayout rlItem;

    }
}