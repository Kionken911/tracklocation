package vn.com.nat.track_location_android.ui.component.splash;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import javax.inject.Inject;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.ui.base.Presenter;
import vn.com.nat.track_location_android.use_case.UserUseCase;
import vn.com.nat.track_location_android.utils.ObjectUtil;


/**
 * Created by NAT on 16/06/2019
 */

public class SplashPresenter extends Presenter<SplashContract.View> implements SplashContract.Presenter {
    private UserUseCase useCase;
    private SharedPreferences sharedPreferences;

    @Inject
    public SplashPresenter(UserUseCase userUseCase) {
        super(userUseCase);
        this.useCase = userUseCase;
        this.sharedPreferences = App.getInstance().getSettingPreference();
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);

    }

    @Override
    public void getExtra(Activity activity) {
        if (ObjectUtil.isNull(activity)) {
            return;
        }

    }

}
