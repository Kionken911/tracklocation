package vn.com.nat.track_location_android;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.VisibleForTesting;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.di.DaggerMainComponent;
import vn.com.nat.track_location_android.di.MainComponent;

import static java.io.File.separator;


/**
 * Created by Dev.TuanNguyen on 12/10/2017.
 */

public class App extends MultiDexApplication {
    private MainComponent mainComponent;
    private static Context context;
    private static App singleton;
    private static String rootPath;
    private static List<Order> orderList = new ArrayList<>();
    public static SharedPreferences sharedPreferences;

    public static String getRootPath() {
        return rootPath;
    }

    public static App getInstance() {
        return singleton;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mainComponent = DaggerMainComponent.create();
        context = getApplicationContext();
        singleton = this;
        rootPath = Environment.getExternalStorageDirectory() + separator + getString(R.string.app_name) + separator;
        sharedPreferences = getSettingPreference();
        FacebookSdk.sdkInitialize(getApplicationContext());
//        // Initialize Places.
//        Places.initialize(getApplicationContext(), "AIzaSyB4IXnMyjPXEJMkv3rk5F2XupsZsukkN3o");
//
//        // Create a new Places client instance.
//        PlacesClient placesClient = Places.createClient(this);
    }

    public void initLocalFolder() {
        File folder = new File(rootPath);
        if (!folder.exists()) {
            folder.mkdir();
        }
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }

    public static Context getContext() {
        return context;
    }

    @VisibleForTesting
    public void setComponent(MainComponent mainComponent) {
        this.mainComponent = mainComponent;
    }

    public SharedPreferences getSettingPreference() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static List<Order> getOrderList() {
        return orderList;
    }
}
