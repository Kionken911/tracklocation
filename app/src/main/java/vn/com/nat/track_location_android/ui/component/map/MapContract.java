package vn.com.nat.track_location_android.ui.component.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;

/**
 * Created by NAT on 16/06/2019
 */

public interface MapContract {
    interface View extends BaseView {
        void setupMapFragment();

        void enableMyLocationIfPermitted();

        void initLocalBroadcastManager();

        void drawPolylinePath(List<LatLng> latLngList);

        void startService();

        void stopService();

        void handleCompleteOrder();

        void checkGPS();

        void buildAlertMessageNoGps();

        void getIntentOrder(Order order,boolean isNewOrder);
    }

    interface Presenter {

    }
}
