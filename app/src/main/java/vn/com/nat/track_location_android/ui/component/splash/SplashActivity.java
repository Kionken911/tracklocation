package vn.com.nat.track_location_android.ui.component.splash;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.ui.base.BaseActivity;
import vn.com.nat.track_location_android.ui.component.home.HomeActivity;

/**
 * Created by NAT on 16/06/2019
 */

public class SplashActivity extends BaseActivity implements SplashContract.View {

    @Inject
    SplashPresenter presenter;

    @Nullable
    @BindView(R.id.imgLogo)
    ImageView imgLogo;

    private AlertDialog dialog = null;

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(SplashActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        presenter.getExtra(this);
        presenter.setActivity(this);
        displayScreen();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @OnClick({})
    public void onClick(View view) {
    }


    @Override
    public void displayScreen() {
        ScaleAnimation fade_in = new ScaleAnimation(0f, 1.5f, 0f, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        fade_in.setDuration(1500);     // animation duration in milliseconds
        fade_in.setFillAfter(true);    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
        imgLogo.startAnimation(fade_in);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateToHomeActivity();
            }
        }, 1500);
    }

    @Override
    public void navigateToHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
