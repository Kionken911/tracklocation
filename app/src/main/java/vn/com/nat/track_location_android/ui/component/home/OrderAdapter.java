package vn.com.nat.track_location_android.ui.component.home;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Order;


/**
 * Created by NAT on 16/06/2019
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private final List<Order> orderList;
    private CustomRecyclerItemListener onItemClickListener;
    private Activity activity;


    public interface CustomRecyclerItemListener {
        void onItemSelected(int position);
    }

    public OrderAdapter(CustomRecyclerItemListener onItemClickListener, List<Order> orderList, Activity activity) {
        this.onItemClickListener = onItemClickListener;
        this.orderList = orderList;
        this.activity = activity;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        return new OrderViewHolder(view, onItemClickListener, activity);
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        holder.bind(position, orderList.get(position), onItemClickListener);

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
