package vn.com.nat.track_location_android.utils;

/**
 * Created by nhanmai on 1/15/18.
 */

public class Settings {
//    public final static String DOMAIN_SERVER = "http://demo10.engma.com.vn";
    public final static String DOMAIN_SERVER = "https://app.cihyn.com";
//    public final static String DOMAIN_SERVER = "http://192.168.1.35";


    public final static String PORT_SERVER = "8080";
//    public final static String PORT_SERVER = "80";

    public final static String URL_SERVER = DOMAIN_SERVER + "/";

    public final static Boolean PRINT_TO_LOGCAT = true;
    public final static Boolean SHOW_ERRrewOR_ALERT = false;
    public final static String TAG_DEBUG = "TAG_DEBUG";
    public final static int SPEED_RUNNING_CARD = 150;
    public final static float SPEED_LINNEAR_MANAGER = 4000f; //Change this value (default=25f)

    public static boolean GET_KEYHASH_RELEASE = false;
    public static boolean DEBUG = true;
}
