package vn.com.nat.track_location_android.data.remote;

import android.support.annotation.NonNull;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import vn.com.nat.track_location_android.App;

import static vn.com.nat.track_location_android.data.remote.ServiceError.NETWORK_ERROR;
import static vn.com.nat.track_location_android.utils.NetworkUtils.isConnected;
import static vn.com.nat.track_location_android.utils.ObjectUtil.isNull;

/**
 * Created by NAT on 5/12/2017
 */

public class RemoteRepository implements RemoteSource {
    private static final int ERROR_UNDEFINED = 401;
    private ServiceGenerator serviceGenerator;
    private final String UNDELIVERABLE_EXCEPTION_TAG = "UNDELIVERABLE_EXCEPTION";

    @Inject
    public RemoteRepository(ServiceGenerator serviceGenerator) {
        this.serviceGenerator = serviceGenerator;
    }

    @NonNull
    private ServiceResponse processCall(Call call, boolean isVoid) {
        if (!isConnected(App.getContext())) {
            return new ServiceResponse(new ServiceError());
        }
        try {
            retrofit2.Response response = call.execute();
            if (isNull(response)) {
                return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
            }
            int responseCode = response.code();
            if (response.isSuccessful()) {
                return new ServiceResponse(responseCode, isVoid ? null : response.body());
            } else {
                return new ServiceResponse(responseCode, isVoid ? null : response.errorBody());
            }
        } catch (IOException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
        }
    }
}
