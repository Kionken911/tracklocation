package vn.com.nat.track_location_android.ui.component.order;

import android.content.Intent;

import java.util.List;

import vn.com.nat.track_location_android.data.remote.dto.Product;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;

/**
 * Created by NAT on 16/06/2019
 */

public interface OrderContract {
    interface View extends BaseView {
        void navigateToAutocompleteSearch();

        void displayAddress(int resultCode, Intent data);

        void displayProduct(List<Product> productList);

        void displayProductDialog();

        void displayProductChoosed(String choosed, List<String> productIdList);

        void navigateToMapActivity();
    }

    interface Presenter {
        void getProductListFake();

        void handleGetProductChoosed(List<Product> productList);
    }
}
