package vn.com.nat.track_location_android.data;


import javax.inject.Inject;

import vn.com.nat.track_location_android.data.local.LocalRepository;
import vn.com.nat.track_location_android.data.remote.RemoteRepository;


/**
 * Created by NAT on 5/12/2017
 */

public class DataRepository implements DataSource {
    private RemoteRepository remoteRepository;
    private LocalRepository localRepository;

    @Inject
    public DataRepository(RemoteRepository remoteRepository, LocalRepository localRepository) {
        this.remoteRepository = remoteRepository;
        this.localRepository = localRepository;
    }

}
