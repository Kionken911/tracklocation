package vn.com.nat.track_location_android.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nhanmai on 3/20/18.
 */

public class TimeUtils {

    /**
     * show Calendar dialog to select a day
     *
     * @param activity
     * @param savedDate
     * @param editText
     * @param invalidMess
     */
    public static void showCalendarDialog(final Activity activity, String savedDate, final EditText editText, final String invalidMess) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat(Constants.DISPLAY_FORMAT_DATE, Locale.US);
        final Calendar c = Calendar.getInstance();
        try {
            c.setTime(dateFormatter.parse(savedDate));      //force Calendar displays saved date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final DatePickerDialog dpd = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();  //current date
                        c.set(year, monthOfYear, dayOfMonth);       //set selected date
                        if (newDate.compareTo(c) >= 0) {//correct format
                            editText.setText(dateFormatter.format(c.getTime()));
                        } else {
                            Toast.makeText(activity, invalidMess, Toast.LENGTH_SHORT).show();
                        }

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }

    /**
     * show Calendar dialog to select a day (birth date)
     *
     * @param activity
     * @param savedDate
     * @param editText
     * @param invalidMess
     * @param dateformat  display format
     */
    public static void showCalendarBirthDateDialog(final Activity activity, Date savedDate, final EditText editText, final String invalidMess, String dateformat) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat(dateformat, Locale.US);
        final Calendar selectdate = Calendar.getInstance();
        if (savedDate != null) {
            selectdate.setTime(savedDate);      //force Calendar displays saved date
        }
        final DatePickerDialog dpd = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar nowDate = Calendar.getInstance();  //current date
                        selectdate.set(year, monthOfYear, dayOfMonth);       //set selected date
                        if (checkBirthDate(selectdate, nowDate)) {
                            editText.setText(dateFormatter.format(selectdate.getTime()));
                        } else {
                            Toast.makeText(activity, invalidMess, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, selectdate.get(Calendar.YEAR), selectdate.get(Calendar.MONTH), selectdate.get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }

    /**
     * show Calendar dialog to select a day (birth date)
     *
     * @param activity
     * @param savedDate
     * @param editText
     * @param invalidMess
     */
    public static void showCalendarBirthDateDialog(Activity activity, Date savedDate, EditText editText,
                                                   String invalidMess) {
        showCalendarBirthDateDialog(activity, savedDate, editText, invalidMess, Constants.DISPLAY_FORMAT_DATE);
    }


    /**
     * check birth date of user
     *
     * @param selectDate date user selected
     * @param nowDate    current date
     * @return
     */
    public static boolean checkBirthDate(Calendar selectDate, Calendar nowDate) {
        if (nowDate.compareTo(selectDate) < 0) {
            return false;
        }
        return true;
    }

    /**
     * @param dateString
     * @return
     */
    public static Date convertDisplayStringToTimeString(String dateString) {
        return convertDisplayStringToTimeString(dateString, Constants.DISPLAY_FORMAT_DATE);
    }


    /**
     * @param dateString
     * @return
     */
    public static Date convertDisplayStringToTimeString(String dateString, String formatDate) {
        SimpleDateFormat format = new SimpleDateFormat(formatDate);
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * @param MilliSeccond
     * @return
     */
    public static String convertLongToTimeString(long MilliSeccond) {
        String dateString = new SimpleDateFormat("dd-MM-yyyy KK:mm a").format(new Date(MilliSeccond));
        return dateString;
    }

    /**
     * @param pubDate
     * @return
     */
    public static String convertPubDateString(String pubDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
        Date formatted = null;
        String formattedString = pubDate;
        try {
            formatted = formatter.parse(pubDate);
            formattedString = formatted.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedString;
    }

    /**
     * @param date
     * @return
     */
    public static String convertFormatDate(String date, String currentFormat, String newFormat) {
        SimpleDateFormat spf = new SimpleDateFormat(currentFormat, Locale.US);
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat(newFormat);
        date = spf.format(newDate);
        return date;
    }
}
