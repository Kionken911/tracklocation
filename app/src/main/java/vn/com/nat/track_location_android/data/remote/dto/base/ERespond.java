package vn.com.nat.track_location_android.data.remote.dto.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nhanmai on 1/18/18.
 */

public class ERespond extends EObject {

    public static final int OK = 200;
    public static final int SERVER_ERROR = 500;
    public static final int SERVER_BAD_REQUEST = 400;
    public static final int SERVER_NOT_FOUND = 404;

    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("message")
    @Expose
    String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
