package vn.com.nat.track_location_android.ui.component.order;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.data.remote.dto.Product;
import vn.com.nat.track_location_android.ui.base.Presenter;
import vn.com.nat.track_location_android.use_case.UserUseCase;
import vn.com.nat.track_location_android.utils.ObjectUtil;


/**
 * Created by NAT on 16/06/2019
 */

public class OrderPresenter extends Presenter<OrderContract.View> implements OrderContract.Presenter {
    private UserUseCase useCase;
    private SharedPreferences sharedPreferences;

    @Inject
    public OrderPresenter(UserUseCase userUseCase) {
        super(userUseCase);
        this.useCase = userUseCase;
        this.sharedPreferences = App.getInstance().getSettingPreference();
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        getProductListFake();
    }

    @Override
    public void getExtra(Activity activity) {
        if (ObjectUtil.isNull(activity)) {
            return;
        }

    }

    @Override
    public void getProductListFake() {
        List<Product> productList = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            Product product = new Product();
            product.setProduct_id("pro" + i);
            product.setProduct_name("product " + i);
            product.setPrice(150000 * i);
            productList.add(product);
        }
        getView().displayProduct(productList);
    }

    @Override
    public void handleGetProductChoosed(List<Product> productList) {
        String choice = "";
        List<String> productIdList = new ArrayList<>();
        if (!ObjectUtil.isEmptyList(productList)) {
            for (Product product : productList) {
                if (product.isChoosed()) {
                    String productName = product.getProduct_name();
                    String productId = product.getProduct_id();
                    choice += productName.concat(", ");
                    productIdList.add(productId);
                }
            }
            getView().displayProductChoosed(choice, productIdList);
        }

    }
}
