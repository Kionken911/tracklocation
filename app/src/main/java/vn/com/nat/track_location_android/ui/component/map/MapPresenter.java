package vn.com.nat.track_location_android.ui.component.map;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import javax.inject.Inject;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.Presenter;
import vn.com.nat.track_location_android.use_case.UserUseCase;
import vn.com.nat.track_location_android.utils.ObjectUtil;
import vn.com.nat.track_location_android.utils.ParamsConstants;


/**
 * Created by NAT on 16/06/2019
 */

public class MapPresenter extends Presenter<MapContract.View> implements MapContract.Presenter {
    private UserUseCase useCase;
    private SharedPreferences sharedPreferences;

    @Inject
    public MapPresenter(UserUseCase userUseCase) {
        super(userUseCase);
        this.useCase = userUseCase;
        this.sharedPreferences = App.getInstance().getSettingPreference();
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);

    }

    @Override
    public void getExtra(Activity activity) {
        if (ObjectUtil.isNull(activity)) {
            return;
        }
        Intent intent = activity.getIntent();

        if (!ObjectUtil.isNull(intent.getExtras())) {
            Order order = intent.getParcelableExtra(ParamsConstants.EXTRA_ORDER);
            boolean isNewOrder = intent.getExtras().getBoolean(ParamsConstants.EXTRA_IS_NEW_ORDER);
            getView().getIntentOrder(order, isNewOrder);
        }

    }

}
