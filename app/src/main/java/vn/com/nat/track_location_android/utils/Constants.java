package vn.com.nat.track_location_android.utils;

/**
 * Created by NAT on 5/12/2017
 */

public class Constants {

    //splash activity
    public static final int SPLASH_DELAY = 1800;

    //ServiceGenerator
    public static final String LANG_VI = "vi";
    public static String CONFIGURATION_FILE = "configuaration.txt";
    //common
    public static final String PHONE_VERSION = "ANDROID ";
    public static final String READ_PHONE_STATE = "READ_PHONE_STATE";
    //format datetime
    public static final String DISPLAY_FORMAT_DATE = "dd-MM-yyyy";
    //Order activity
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1010;
    //map
    public static final long UPDATE_GEOLOCATION_DELAY_TIME = 10 * 1000;//10s
    public static final long FASTEST_LOCATION_INTERVAL = 2000;//2s
    public static final String HANDLER_SEND_GPS = "handler_send_gps";
    public static final int SIZE_ZOOM_MAP = 13;
}
