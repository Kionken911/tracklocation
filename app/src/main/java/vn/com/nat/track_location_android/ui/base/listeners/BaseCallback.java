package vn.com.nat.track_location_android.ui.base.listeners;


import vn.com.nat.track_location_android.data.remote.dto.base.ERespond;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface BaseCallback {
    void onSuccess(ERespond eRespond);

    void onFail(Throwable error);

}
