package vn.com.nat.track_location_android.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.BuildConfig;

import static java.io.File.separator;


/**
 * Created by Dev.TuanNguyen on 12/16/2017.
 */

public class CommonUtils {

    public static void showAppToast(final Activity activity, final int resId) {
        if (activity != null && activity.getString(resId) != null) {
            showAppToast(activity, activity.getString(resId));
        }
    }

    public static void showAppToast(final Activity activity, final String message) {
        if (activity != null && !TextUtils.isEmpty(message)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    public static void showAlert(Context context, String title, String message, String negativeButton, String positiveButton, Runnable yesCallback, Runnable noCallback) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // continue with delete
                        if (yesCallback != null) {
                            yesCallback.run();
                        }
                    }
                })
                .setPositiveButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (noCallback != null) {
                            noCallback.run();
                        }
                        return;
                    }
                }).show();
    }

    public static boolean updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return true;
    }

    public static void writeLog(String data, String appName, String fileName) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + separator + appName + separator
                        );

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, fileName);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            try {
                file.createNewFile();

            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
        try {
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(data);
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean saveToFile( String data){
        try {
            File file = new File(App.getRootPath() + Constants.CONFIGURATION_FILE);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file,true);
            fileOutputStream.write((data).getBytes());

            return true;
        }  catch(FileNotFoundException ex) {
            Log.d("saveToFile", ex.getMessage());
        }  catch(IOException ex) {
            Log.d("saveToFile", ex.getMessage());
        }
        return  false;


    }

    public static String readFile(Context context) {
        String line = null;

        try {
            FileInputStream fileInputStream = new FileInputStream(new File(App.getRootPath() + Constants.CONFIGURATION_FILE));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            fileInputStream.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            Log.d("readFile", ex.getMessage());
        } catch (IOException ex) {
            Log.d("readFile", ex.getMessage());
        }
        return line;
    }

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return Constants.PHONE_VERSION + sdkVersion + " (" + release + ")";
    }

    public static String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static String getFormatNumber(int total) {
        if (total > 0) {
            NumberFormat format = new DecimalFormat("#,###,###");
            String formatedNumber = format.format(total);
            return formatedNumber;

        } else {
            return "0";
        }
    }

    public static String boldTextByHtml(String s) {
        return "<b>" + s + "</b>";
    }

    public static Map<String, String> sendIdPhone(Map<String, String> params, Context context) throws JSONException {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String push_firebase_token = FirebaseInstanceId.getInstance().getToken();

        SharedPreferences sharedPreferences = App.getInstance().getSettingPreference();
        if (tManager != null) {
            String uid = "";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                uid = tManager.getDeviceId();
                sharedPreferences.edit().putString(Constants.READ_PHONE_STATE, uid).apply();
            } else {
                String deviceId = tManager.getDeviceId();
                if (!TextUtils.isEmpty(deviceId)) {
                    uid = deviceId;
                    sharedPreferences.edit().putString(Constants.READ_PHONE_STATE, uid).apply();
                }
            }
            if (TextUtils.isEmpty(uid)) {
                uid = sharedPreferences.getString(Constants.READ_PHONE_STATE, push_firebase_token);
            }

            params.put(ParamsConstants.PARAM_DEVICE_ID, uid);
            params.put(ParamsConstants.PARAM_PHONE_NAME, Constants.PHONE_VERSION + getDeviceModel());
            params.put(ParamsConstants.PARAM_PHONE_VERSION, getAndroidVersion());

            params.put(ParamsConstants.PARAM_APPLICATION_ID, BuildConfig.APPLICATION_ID);
            params.put(ParamsConstants.PARAM_APP_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        }
        params.put(ParamsConstants.PUSH_FIREBASE_TOKEN, push_firebase_token);

        TimeZone tz = TimeZone.getDefault();
        params.put(ParamsConstants.TIMEZONE, tz.getID());

        return params;
    }

    public static String hashPassword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return SecurityUtils.hashSHA256(password);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean isUsernameValid(String username) {
        boolean isvalid = false;
        String expression = "^[a-zA-Z][a-zA-Z0-9._-]{2,45}$"; //first character must be alphabet and String must be 3 characters at least
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        CharSequence inputStr = username;
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches() && username.length() >= 3) {
            isvalid = true;
        }
        return isvalid;
    }


    public static boolean isPasswordValid(String password) {
        boolean isValid = true;
        if (password.length() < 6 || password.indexOf(' ') > -1) {
            return false;
        }
        return isValid;
    }

    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPhoneValid(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public static String normalizePhoneNumber(String number, String country_code) {
        number = number.replaceAll("[^+0-9]", ""); // All weird characters such as /, -, ...
        if (number.substring(0, 1).compareTo("0") == 0 && number.substring(1, 2).compareTo("0") != 0) {
            number = "+" + country_code + number.substring(1); // e.g. 0172 12 34 567 -> + (country_code) 172 12 34 567
        } else {
            if (number.substring(0, 1).compareTo("0") != 0) {
                number = "+" + country_code + number; // e.g. 0172 12 34 567 -> + (country_code) 172 12 34 567
            }
        }
        number = number.replaceAll("^[0]{1,4}", "+"); // e.g. 004912345678 -> +4912345678
        return number;
    }

    private static boolean isValidFullname(String fullName) {
        boolean isValid = true;
        if (fullName.length() < 4 || fullName.length() > 30) {
            return false;
        }
        return isValid;
    }

    public static void setLanguage(Context context, String language) {

        Locale myLocale = new Locale(language);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public static String getPathFile(Uri uri, Activity activity) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("FILE".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    public static String getFileNameFromUrl(String url) {
        return url.substring(url.lastIndexOf(separator) + 1);
    }

    public static boolean getFilefromURL(String url, String folderName) {
        String fileName = getFileNameFromUrl(url);
        File saveDir = new File(App.getRootPath() + folderName);
        if (!saveDir.exists()) {
            saveDir.mkdir();
        }
        File file = new File(saveDir, fileName);
        URL imageUrl = null;
        try {
            imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();

            // Constructs a new FileOutputStream that writes to file
            // if file not exist then it will create file
            OutputStream os = new FileOutputStream(file);

            // See Utils class CopyStream method
            // It will each pixel from input stream and
            // write pixels to output stream (file)
            CopyStream(is, os);

            os.close();
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {

            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                //Read byte from input stream

                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;

                //Write byte from output stream
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static Bitmap convertString2Bitmap(String imgString) {
        byte[] decodedString = Base64.decode(imgString, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

}

