package vn.com.nat.track_location_android.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.ui.base.listeners.BaseView;
import vn.com.nat.track_location_android.ui.component.map.MapActivity;
import vn.com.nat.track_location_android.utils.CommonUtils;
import vn.com.nat.track_location_android.utils.NetworkUtils;


/**
 * Created by Dev.TuanNguyen on 12/10/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {

    protected Presenter presenter;
    private Unbinder unbinder;

    ProgressDialog progressDialog;

    protected BaseActivity activity;
    protected App app;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        {
            View layout = null;
            Bundle bundle = getArguments();
            activity = (MapActivity) getActivity();
            app = (App) activity.getApplicationContext();
            layout = inflater.inflate(getLayoutId(), container, false);
            unbinder = ButterKnife.bind(this, layout);
            initializeDagger();
            initializePresenter();
            if (presenter != null) {
                presenter.setActivity(activity);
                presenter.setFragment(this);
                presenter.initialize(bundle);
            }

            return layout;
        }
    }


    protected abstract void initializeDagger();

    protected abstract void initializePresenter();

    public abstract int getLayoutId();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showToast(int errMessage) {
        CommonUtils.showAppToast(getActivity(), errMessage);
    }


    @Override
    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    @Override
    public boolean isInternetConnected() {
        boolean isConnected = true;
        if (!NetworkUtils.isConnected(getContext())) {
            isConnected = false;
            showToast(R.string.err_no_connected);
        }
        return isConnected;
    }

}