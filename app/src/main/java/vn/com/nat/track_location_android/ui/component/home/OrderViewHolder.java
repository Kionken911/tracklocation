package vn.com.nat.track_location_android.ui.component.home;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.utils.ObjectUtil;


/**
 * Created by NAT on 16/06/2019
 */

public class OrderViewHolder extends RecyclerView.ViewHolder {

    private OrderAdapter.CustomRecyclerItemListener onItemClickListener;

    @Nullable
    @BindView(R.id.card_view)
    CardView card_view;

    @Nullable
    @BindView(R.id.imgCustomerAvatar)
    ImageView imgProfileAvatar;

    @Nullable
    @BindView(R.id.tvName)
    TextView tvName;

    @Nullable
    @BindView(R.id.tvAddress)
    TextView tvAddress;

    @Nullable
    @BindView(R.id.tvDate)
    TextView tvDate;

    private Activity activity;


    public OrderViewHolder(View itemView, OrderAdapter.CustomRecyclerItemListener onItemClickListener, Activity activity) {
        super(itemView);
        this.activity = activity;
        ButterKnife.bind(this, itemView);
        this.onItemClickListener = onItemClickListener;
    }

    public void bind(int position, Order order, OrderAdapter.CustomRecyclerItemListener recyclerItemListener) {
        //need to move to mapper
        if (!ObjectUtil.isNull(order) && !ObjectUtil.isEmptyStr(order.getCustome_name())) {
            String customeName = order.getCustome_name();
            tvName.setText(customeName);
        }
        if (!ObjectUtil.isNull(order) && !ObjectUtil.isEmptyStr(order.getCompleteDate())) {
            String completeDate = order.getCompleteDate();
            tvDate.setText(completeDate);
        }
        if (!ObjectUtil.isNull(order) && !ObjectUtil.isEmptyStr(order.getAddress())) {
            String address = order.getAddress();
            tvAddress.setText(address);
        }

        if (!ObjectUtil.isNull(card_view)) {
            card_view.setOnClickListener(v -> recyclerItemListener.onItemSelected(position));
        }

    }

}

