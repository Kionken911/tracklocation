package vn.com.nat.track_location_android.ui.base.listeners;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface BaseView {
    void showToast(int errMessage);

    boolean isInternetConnected();

    void dismissDialog();

}
