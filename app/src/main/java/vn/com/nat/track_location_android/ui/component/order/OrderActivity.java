package vn.com.nat.track_location_android.ui.component.order;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.data.remote.dto.Product;
import vn.com.nat.track_location_android.ui.base.BaseActivity;
import vn.com.nat.track_location_android.ui.component.map.MapActivity;
import vn.com.nat.track_location_android.utils.CommonUtils;
import vn.com.nat.track_location_android.utils.Constants;
import vn.com.nat.track_location_android.utils.ObjectUtil;
import vn.com.nat.track_location_android.utils.ParamsConstants;

/**
 * Created by NAT on 16/06/2019
 */

public class OrderActivity extends BaseActivity implements OrderContract.View {

    @Inject
    OrderPresenter presenter;

    @Nullable
    @BindView(R.id.etAddress)
    EditText etAddress;

    @Nullable
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;

    @Nullable
    @BindView(R.id.tvProductList)
    TextView tvProductList;

    @Nullable
    @BindView(R.id.tvStart)
    TextView tvStart;

    @Nullable
    @BindView(R.id.imgBack)
    ImageView imgBack;

    List<Product> productList = new ArrayList<>();
    private ProductAdapter productAdapter;
    private List<String> productIdList = new ArrayList<>();

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(OrderActivity.this);
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        presenter.getExtra(this);
        presenter.setActivity(this);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_order;
    }

    @OnClick({R.id.tvProductList, R.id.tvStart, R.id.imgBack})
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.tvAddress: {
//                //search by google map tool
//                navigateToAutocompleteSearch();
//                break;
//            }
            case R.id.tvProductList: {
                displayProductDialog();
                break;
            }
            case R.id.tvStart: {
                navigateToMapActivity();
                break;
            }
            case R.id.imgBack: {
                onBackPressed();
                break;
            }
        }
    }

    @Override
    public void navigateToAutocompleteSearch() {
        AutocompleteFilter filter =
                new AutocompleteFilter.Builder().setCountry("VN").build();
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(filter)
                            .build(this);
            startActivityForResult(intent, Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE);

        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void displayAddress(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(this, data);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    etAddress.setText(place.getAddress());
                }
            });
        }
    }

    @Override
    public void displayProduct(List<Product> data) {
        productList = data;
    }

    @Override
    public void displayProductDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_product, null);
        dialogBuilder.setView(dialogView);

        ListView lvProduct = dialogView.findViewById(R.id.lvProduct);
        TextView tvAccept = dialogView.findViewById(R.id.tvAccept);
        TextView tvCancel = dialogView.findViewById(R.id.tvCancel);

        ProductAdapter.ProductItemListener listener = new ProductAdapter.ProductItemListener() {
            @Override
            public void onItemSelected(int position) {
                Product product = productList.get(position);
                if (product.isChoosed()) {
                    productList.get(position).setChoosed(false);
                } else {
                    productList.get(position).setChoosed(true);
                }
                if (!ObjectUtil.isNull(productAdapter)) {
                    productAdapter.notifyDataSetChanged();
                }
            }
        };
        productAdapter = new ProductAdapter(OrderActivity.this, R.layout.item_product, productList, listener);
        lvProduct.setAdapter(productAdapter);


        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ObjectUtil.isNull(presenter)) {
                    presenter.handleGetProductChoosed(productList);
                    alertDialog.dismiss();
                }

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void displayProductChoosed(String result, List<String> productIdList) {
        if (!ObjectUtil.isEmptyStr(result)) {
            result = result.substring(0, result.length() - 2);
        }
        this.productIdList = productIdList;
        tvProductList.setText(result);
    }

    @Override
    public void navigateToMapActivity() {
        String customerName = etCustomerName.getText().toString();
        if (ObjectUtil.isEmptyStr(customerName)) {
            CommonUtils.showAppToast(this, R.string.empty_customer_name);
            return;
        }
        String productList = tvProductList.getText().toString();
        if (ObjectUtil.isEmptyStr(productList)) {
            CommonUtils.showAppToast(this, R.string.empty_product_list);
            return;
        }
        String address = etAddress.getText().toString();
        if (ObjectUtil.isEmptyStr(address)) {
            CommonUtils.showAppToast(this, R.string.empty_address);
            return;
        }
        Order order = new Order();
        order.setCustome_name(customerName);
        order.setAddress(address);
        order.setProduct_list(productIdList);
        Intent intent = new Intent(this, MapActivity.class);
//        intent.putExtra(“parcel_data”, movie);
        intent.putExtra(ParamsConstants.EXTRA_ORDER, (Parcelable) order);
        intent.putExtra(ParamsConstants.EXTRA_IS_NEW_ORDER, true);
        startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
//            displayAddress(resultCode, data);
        }
    }
}
