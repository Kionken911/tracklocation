package vn.com.nat.track_location_android.data.remote.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import vn.com.nat.track_location_android.data.remote.dto.base.EObject;
import vn.com.nat.track_location_android.data.remote.dto.base.ERespond;
import vn.com.nat.track_location_android.utils.TimeUtils;


/**
 * Created by NAT on 16/06/2019
 */

public class Order extends EObject implements Parcelable {

    protected Order(Parcel in) {
        custome_name = in.readString();
        address = in.readString();
        complete_time = in.readLong();
        locate_list = in.createTypedArrayList(LatLng.CREATOR);
        product_list = in.createStringArrayList();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(custome_name);
        dest.writeString(address);
        dest.writeLong(complete_time);
        dest.writeTypedList(locate_list);
        dest.writeStringList(product_list);
    }

    public static class OrderRespond extends ERespond {
        Order data;

        public Order getData() {
            return data;
        }

        public void setData(Order data) {
            this.data = data;
        }
    }

    @Expose
    @SerializedName("custome_name")
    private String custome_name;
    @Expose
    @SerializedName("address")
    private String address;
    @Expose
    @SerializedName("complete_time")
    private long complete_time;
    @Expose
    @SerializedName("locate_list")
    private List<LatLng> locate_list = new ArrayList<>();
    @Expose
    @SerializedName("product_list")
    private List<String> product_list = new ArrayList<>();

    public Order() {
    }

    public Order(String custome_name, String address, long complete_time, List<LatLng> locate_list, List<String> product_list) {
        this.custome_name = custome_name;
        this.address = address;
        this.complete_time = complete_time;
        this.locate_list = locate_list;
        this.product_list = product_list;
    }

    public String getCustome_name() {
        return custome_name;
    }

    public void setCustome_name(String custome_name) {
        this.custome_name = custome_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<LatLng> getLocate_list() {
        return locate_list;
    }

    public void setLocate_list(List<LatLng> locate_list) {
        this.locate_list = locate_list;
    }

    public List<String> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List<String> product_list) {
        this.product_list = product_list;
    }

    public long getComplete_time() {
        return complete_time;
    }

    public void setComplete_time(long complete_time) {
        this.complete_time = complete_time;
    }

    public String getCompleteDate() {
        return TimeUtils.convertLongToTimeString(complete_time);
    }
}
