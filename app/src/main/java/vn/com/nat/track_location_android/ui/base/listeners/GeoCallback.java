package vn.com.nat.track_location_android.ui.base.listeners;

import vn.com.nat.track_location_android.data.remote.dto.base.GeoRespond;

/**
 * Created by tuannguyen on 13/12/17.
 */

public interface GeoCallback {
    void onSuccess(GeoRespond eRespond);

    void onFail(Throwable error);

}
