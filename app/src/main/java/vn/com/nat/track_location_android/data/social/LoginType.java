package vn.com.nat.track_location_android.data.social;

/**
 * Created by nhanmai on 1/16/18.
 */

public enum LoginType {
    GOOGLE, FACEBOOK, NORMAL
}
