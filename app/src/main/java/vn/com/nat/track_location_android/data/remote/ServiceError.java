package vn.com.nat.track_location_android.data.remote;

/**
 * Created by NAT on 5/12/2017
 */

public class ServiceError {
    public static final String NETWORK_ERROR = "Unknown ServiceError";
    public static final String WRONG_CAPTCHA = "WRONG_CAPTCHA";
    public static final String USER_IS_EXISTED = "USER_IS_EXISTED";
    public static final String USER_IS_NOT_VERIFIED = "USER_IS_NOT_VERIFIED";
    public static final String EMAIL_IS_EXISTED = "EMAIL_IS_EXISTED";
    public static final String PHONE_IS_EXISTED = "PHONE_IS_EXISTED";
    public static final String INVALID_USERNAME_OR_PASSWORD = "INVALID_USERNAME_OR_PASSWORD";
    public static final String USER_IS_NOT_ACTIVATED = "USER_IS_NOT_ACTIVATED";
    public static final String USER_IS_BANNED = "USER_IS_BANNED";
    public static final String USER_IS_NOT_EXISTED = "USER_IS_NOT_EXISTED";
    public static final String INVALID_TOKEN = "INVALID_TOKEN";
    public static final String INVALID_CURRENT_PASSWORD = "INVALID_CURRENT_PASSWORD";
    public static final String NOT_LOGIN = "NOT_LOGIN";
    public static final String PHONE_IS_DUPLICATED_MSG = "PHONE_IS_DUPLICATED_MSG";
    public static final String LIMIT_IMAGE_SERVICE = "LIMIT_IMAGE_SERVICE";
    public static final String REFERRER_PHONE_IS_NOT_EXISTED = "REFERRER_PHONE_IS_NOT_EXISTED";
    public static final String PHONE_IS_DUPLICATED = "PHONE_IS_DUPLICATED";
    public static final String EMAIL_IS_NOT_EXISTED = "EMAIL_IS_NOT_EXISTED";
    public static final String CERTIFICATE_NUMBER_EXISTED = "CERTIFICATE_NUMBER_EXISTED";
    private static final int GROUP_200 = 2;
    private static final int GROUP_400 = 4;
    private static final int GROUP_500 = 5;
    private static final int VALUE_100 = 100;
    public static final int SUCCESS_CODE = 200;
    public static final int SERVER_ERROR_CODE = 500;
    public static final int ERROR_CODE = 400;
    private String description;
    private int code;

    public static boolean isSuccess(int responseCode) {
        return responseCode / VALUE_100 == GROUP_200;
    }

    public static boolean isClientError(int errorCode) {
        return errorCode / VALUE_100 == GROUP_400;
    }

    public static boolean isServerError(int errorCode) {
        return errorCode / VALUE_100 == GROUP_500;
    }

    public ServiceError() {
    }

    public ServiceError(String description, int code) {
        this.description = description;
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }
}
