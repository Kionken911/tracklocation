package vn.com.nat.track_location_android.ui.component.home;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.Presenter;
import vn.com.nat.track_location_android.use_case.UserUseCase;
import vn.com.nat.track_location_android.utils.ObjectUtil;


/**
 * Created by NAT on 16/06/2019
 */

public class HomePresenter extends Presenter<HomeContract.View> implements HomeContract.Presenter {
    private UserUseCase useCase;
    private SharedPreferences sharedPreferences;

    @Inject
    public HomePresenter(UserUseCase userUseCase) {
        super(userUseCase);
        this.useCase = userUseCase;
        this.sharedPreferences = App.getInstance().getSettingPreference();
    }

    @Override
    public void initialize(Bundle extras) {
        super.initialize(extras);
        if (ObjectUtil.isEmptyList(App.getOrderList())) {
//            getOrderListFake();
        } else {
            getView().displayOrderList(App.getOrderList());
        }

    }

    @Override
    public void getExtra(Activity activity) {
        if (ObjectUtil.isNull(activity)) {
            return;
        }

    }

    @Override
    public void getOrderListFake() {
        List<Order> orderList = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            Order order = new Order();
            order.setCustome_name("Customer" + i);
            order.setAddress("12/16 Dao Duy Anh, phuong 9, Phu Nhuan,HCM");
            order.setComplete_time(System.currentTimeMillis());
            orderList.add(order);
        }
        getView().displayOrderList(orderList);
    }
}
