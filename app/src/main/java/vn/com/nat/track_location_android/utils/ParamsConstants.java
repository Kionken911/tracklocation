package vn.com.nat.track_location_android.utils;

/**
 * Created by nhanmai on 7/12/17.
 */

public class ParamsConstants {
    //common
    public static final String PARAM_PHONE_VERSION = "device_version";
    public static final String PARAM_PHONE_NAME = "device_name";
    public static final String PARAM_HASH_KEY = "hash_key";
    public static final String PARAM_HASH_KEY_FB = "hash_key_fb";
    public static final String PARAM_APPLICATION_ID = "app_id";
    public static final String PARAM_APP_VERSION = "app_version";
    public static final String PARAM_FILE = "file";
    public static final String PARAM_CROP_FILE = "crop_file";
    public static final String TIMEZONE = "timezone";
    public static final String PARAM_DEVICE_ID = "device_id";
    public static final String PARAM_JWT = "jwt";

    //User
    public static final String USERNAME = "username";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PASSWORD = "password";
    public static final String CAPTCHA = "captcha";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TOKEN = "social_token";
    public static final String SOCIAL_IMG_SOURCE = "social_img_src";
    public static final String FULLNAME = "fullname";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    public static final String PHONE = "phone";
    public static final String PHONE_REF = "phone_ref";
    public static final String ADDRESS = "address";

    //header
    public static final String PARAM_TIMEZONE = "timezone";
    public static final String PARAM_LANGUAGE = "Language";
    public static final String PARAM_AUTHORIZATION = "Authorization";

    //user info shared preferences
    public static final String PREF_LANGUAGE = "language";
    public static final String PREF_JWT = "PREF_JWT";
    public static final String PREF_FULLNAME = "PREF_FULLNAME";
    public static final String PREF_PHONE = "PREF_PHONE";
    public static final String PREF_PHONE_CODE = "PREF_PHONE_CODE";
    public static final String PREF_PHONE_CODE_REF = "PREF_PHONE_CODE_REF";
    public static final String PREF_PHONE_REF = "PREF_PHONE_REF";
    public static final String PREF_COUNTRY = "PREF_COUNTRY";
    public static final String PREF_YOUR_COMPANY = "PREF_YOUR_COMPANY";
    public static final String PREF_BADGE_NUMBER = "PREF_BADGE_NUMBER";
    public static final String PREF_EMAIL = "PREF_EMAIL";
    public static final String PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE";
    public static final String PREF_SOCIAL_TYPE = "PREF_SOCIAL_TYPE";
    public static final String PREF_TYPE_USER = "PREF_TYPE_USER";
    public static final String PREF_USER_ID = "PREF_USER_ID";

    //push notification
    public static final String PUSH_FIREBASE_TOKEN = "firebase_token";

    public static final String PARAM_BEARER = "Bearer";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_EXTRA_LINK = "PARAM_EXTRA_LINK";
    public static final String PARAM_URL = "url";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_LAST_ID = "last_id";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_NOTIFICATION_LIST = "notification_ids";
    public static final String PARAM_PHONE_ID = "phone_id";

    //params user
    public static final String PARAM_TYPE_USER = "Typeuser";


    public static final String PARAM_EXTRA_SERVICE_ID = "PARAM_EXTRA_SERVICE_ID";
    public static final String PARAM_EXTRA_CATEGORY_ID = "PARAM_EXTRA_CATEGORY_ID";
    public static final String PARAM_CATEGORY_ID = "category_id";
    public static final String PARAM_SEARCH = "search";
    public static final String PARAM_TYPE_RECORD = "type_record";
    public static final String SERVICE_ID = "service_id";
    public static final String PARAM_TIME_DELIVERY = "time_delivery";
    public static final String PARAM_NOTE = "note";
    public static final String PARAM_ORDER_LOCATION = "order_location";
    public static final String PARAM_TYPE_USER_ID = "type_user_id";
    public static final String PARAM_IMAGE_RECORD = "image_record";
    public static final String PARAM_DATA_RECORD = "data_record";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_IMG = "img";
    public static final String PARAM_EXTRA_ORDER_ID = "PARAM_EXTRA_ORDER_ID";
    public static final String PARAM_VO_ID = "vo_id";
    public static final String PARAM_ORDER_ID = "order_id";
    public static final String PARAM_ORDER_VO_ID = "order_vo_id";
    public static final String PARAM_ORDER_ITEM_ID = "order_item_id";
    public static final String PARAM_RATING = "rating";
    public static final String PARAM_RESULT_STATUS = "reslut_status";
    public static final String PARAM_ERROR_MESSAGE = "error_message";
    public static final String PARAM_CODE = "code";
    public static final String PARAM_VOUCHER_CODE = "voucher_code";
    public static final String PREF_IS_LOGIN_SOCIAL = "PREF_IS_LOGIN_SOCIAL";
    public static final String PARAM_PAYMENT_TYPE = "payment_type";
    public static final String PARAM_SALE_PRICE = "sale_price";
    public static final String PARAM_EXTRA_IS_PUSH = "PARAM_EXTRA_IS_PUSH";
    public static final String PREF_ACCOUNT_TYPE = "PREF_ACCOUNT_TYPE";
    public static final String PARAM_PROVINCE_ID = "province_id";
    public static final String PARAM_DISTRICT_ID = "district_id";
    public static final String PARAM_WARD_ID = "ward_id";
    public static final String WARNING_CODE = "WARNING_CODE";
    public static final String PREF_POINT = "PREF_POINT";
    public static final String SHORT_NAME_PHONE = "short_name_phone";
    public static final String SHORT_NAME_PHONE_REF = "short_name_phone_ref";
    public static final String CERTIFICATE_NUMBER = "certificate_number";
    public static final String SERVICES = "services";
    public static final String ABOUT = "about";
    public static final String IMAGES = "images";
    public static final String PASSWORD_NEW = "new_password";
    public static final String PASSWORD_CURRENT = "current_password";
    public static final String PREF_DISTANCE = "PREF_DISTANCE";
    public static final String PREF_MEDAL = "PREF_MEDAL";
    public static final String PREF_CERTIFICATE = "PREF_CERTIFICATE";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "long";
    public static final String LATITUDE_TECH = "lat_tech";
    public static final String LONGITUDE_TECH = "long_tech";
    public static final String CATEGORY_KEY = "category_key";
    public static final String MEDAL_KEY = "medal_key";
    public static final String EXTRA_ACCOUNT_ID = "EXTRA_ACCOUNT_ID";
    public static final String ACCOUNT_ID = "account_id";
    public static final String PREF_IS_FINDING_JOB = "PREF_IS_FINDING_JOB";
    public static final String PARAM_EXECUTOR_ID = "executor_id";
    public static final String PARAM_MEDAL_KEY = "medal_key";
    public static final String PARAM_EXTRA_NOTIFICATION = "PARAM_EXTRA_NOTIFICATION";

    //dienquang_ver2
    public static final String PARAM_USER_NAME = "username";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_HANDLER = "handler";
    public static final String PARAM_REDIRECT_URI = "redirect_uri";
    public static final String PREF_URL_LOGGED = "PREF_URL_LOGGED";
    public static final String PREF_SERVER_DOMAIN = "PREF_SERVER_DOMAIN";
    public static final String EXTRA_ORDER = "EXTRA_ORDER";
    public static final String EXTRA_IS_NEW_ORDER = "EXTRA_IS_NEW_ORDER";
}
