package vn.com.nat.track_location_android.ui.component.home;


import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import vn.com.nat.track_location_android.App;
import vn.com.nat.track_location_android.R;
import vn.com.nat.track_location_android.data.remote.dto.Order;
import vn.com.nat.track_location_android.ui.base.BaseActivity;
import vn.com.nat.track_location_android.ui.component.map.MapActivity;
import vn.com.nat.track_location_android.ui.component.order.OrderActivity;
import vn.com.nat.track_location_android.utils.ObjectUtil;
import vn.com.nat.track_location_android.utils.ParamsConstants;

/**
 * Created by NAT on 16/06/2019
 */

public class HomeActivity extends BaseActivity implements HomeContract.View {

    @Inject
    HomePresenter presenter;

    @Nullable
    @BindView(R.id.rvOrderList)
    RecyclerView rvOrderList;

    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private OrderAdapter orderAdapter;
    private List<Order> orderList = new ArrayList<>();
    public Order selectedOrder;

    @Override
    protected void initializeDagger() {
        App app = (App) getApplicationContext();
        app.getMainComponent().inject(HomeActivity.this);
        initOrderList();
    }

    @Override
    protected void initializePresenter() {
        super.presenter = presenter;
        presenter.setView(this);
        presenter.getExtra(this);
        presenter.setActivity(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @OnClick({R.id.fab})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab: {
                navigateToOrderActivity();
                break;
            }
        }
    }

    @Override
    public void navigateToOrderActivity() {
        Intent intent = new Intent(this, OrderActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToMapActivity(Order order) {
        this.selectedOrder = order;
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(ParamsConstants.EXTRA_ORDER, (Parcelable) selectedOrder);
        intent.putExtra(ParamsConstants.EXTRA_IS_NEW_ORDER, false);
        startActivity(intent);
    }

    @Override
    public void initOrderList() {
        OrderAdapter.CustomRecyclerItemListener customRecyclerItemListener = new OrderAdapter.CustomRecyclerItemListener() {
            @Override
            public void onItemSelected(int position) {
                //show track location
                Order order = orderList.get(position);
                if (!ObjectUtil.isNull(order)) {
                    navigateToMapActivity(order);
                }

            }
        };
        orderAdapter = new OrderAdapter(customRecyclerItemListener, orderList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvOrderList.setLayoutManager(layoutManager);
        rvOrderList.setHasFixedSize(true);
        rvOrderList.setItemAnimator(null);
        rvOrderList.setAdapter(orderAdapter);
    }

    @Override
    public void displayOrderList(List<Order> data) {
        orderList.addAll(data);
        orderAdapter.notifyDataSetChanged();
    }
}
