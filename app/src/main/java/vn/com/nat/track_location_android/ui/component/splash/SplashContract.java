package vn.com.nat.track_location_android.ui.component.splash;

import vn.com.nat.track_location_android.ui.base.listeners.BaseView;

/**
 * Created by NAT on 16/06/2019
 */

public interface SplashContract {
    interface View extends BaseView {
        void displayScreen();

        void navigateToHomeActivity();
    }

    interface Presenter {

    }
}
